<?php

namespace App\Models\Farm;

use App\Http\Controllers\Controller;
use App\Models\DesignPatterns\FarmRequestFactory;
use App\Models\DesignPatterns\FarmRequestMethod;
use App\Models\DesignPatterns\Singleton;
use App\Models\LabYak\LabYak;
use App\Models\LabYak\LabYakCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class FarmBaseService extends Controller implements BaseService
{

    use Singleton;

    private FarmRequestMethod $requestMethod;

     function __construct()
    {
    }

    static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new self();
        }
        return static::$instance;
    }

    /**
     * @param Request $request
     * @param int $passedDays
     */
    function processOrder(Request $request, $passedDays)
    {
        $stock = $this->getPredictedStockDataByDayFromCollection(LabYakCollection::fromXML(), $passedDays);
        return $this->setRequestMethod(FarmRequestFactory::getRequestProcessingMethod($request, $stock))
            ->requestMethod->handle($request);
    }

    /**
     * @param Collection $labYakCollection
     * @param int $passedDays
     * @return array
     */
    public function getPredictedStockDataByDayFromCollection(Collection $labYakCollection, int $passedDays): array
    {
        $stock = [
            'herd' => [],
            'skin_units' => [],
            'milk_liters' => [],
            'milk_liters_total' => 0.0,
            'skin_units_total' => 0,
            'passedDays' => $passedDays
        ];
        $labYakCollection->each(function (LabYak $labYabk, $key) use ($passedDays, &$stock) {
            $shaveYakOnDay = $labYabk->getNextShaveDay();
            $ageLastShaved = $labYabk->getAgeInDays();
            for ($i = 0; $i < $passedDays; $i++) {
                $canShave = ($labYabk->eligibleForFirstShaveJob() || $shaveYakOnDay === $i);
                if ($canShave) {
                    $ageLastShaved = $labYabk->getAgeInDays();
                    $shaveYakOnDay = $labYabk->getNextShaveDay();
                    $labYabk->hadFirstShaveJob = true;
                    $extractedSkin = $labYabk->shave();
                    $stock['skin_units_total'] += $extractedSkin;
                    $lastShaveDay = $labYabk->getAgeInDays() + 1;
                    $stock['skin_units'][$labYabk->getName()][] = [
                        'amount' => $extractedSkin,
                        'shaved_on_day' => $lastShaveDay
                    ];
                }
                $extractedMilk = $labYabk->milk();
                $stock['milk_liters'][$labYabk->getName()][] = $extractedMilk;
                $stock['milk_liters_total'] += $extractedMilk;
                $labYabk->growOld();

                if ($i + 1 === $passedDays) {
                    $stock['herd'][] = [
                        'name' => $labYabk->getName(),
                        'age' => $labYabk->getAge(),
                        'age-last-shaved' =>  number_format(($ageLastShaved  / 100),1)
                    ];
                }
            }
        });
        return $stock;
    }

    function print(Request $request, int $passedDays)
    {
        $herd = $this->getPredictedStockDataByDayFromCollection(LabYakCollection::fromXML(), $passedDays);
        $this->setRequestMethod(FarmRequestFactory::getInstance()->getRequestProcessingMethod($request, $herd));

        $this->requestMethod->setRequestPayloadData($herd);
        return $this->requestMethod->handle($request);
    }

    function getRequestMethod(Request $request, array $data): FarmRequestMethod
    {
        return FarmRequestFactory::getInstance()->getRequestProcessingMethod($request, $data);
    }

    /**
     * @param FarmRequestMethod $requestMethod
     * @return FarmBaseService
     */
    public function setRequestMethod(FarmRequestMethod $requestMethod): FarmBaseService
    {
        $this->requestMethod = $requestMethod;
        return $this;
    }

    /**
     * @param Collection $collection
     * @param int $passedDays
     * @return Collection
     */
    function getPredictedHerdDataByPassedDays(Collection $collection, int $passedDays): Collection
    {
        return $collection->map(function (LabYak $labYabk, $key) use ($passedDays) {
            $labYabk->setAgeInDays($labYabk->getAgeInDays() + $passedDays);
            return $labYabk;
        });
    }
}
