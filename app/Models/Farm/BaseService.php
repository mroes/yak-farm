<?php

namespace App\Models\Farm;

use App\Models\DesignPatterns\FarmRequestMethod;
use Illuminate\Http\Request;

interface BaseService
{

    function processOrder(Request $request, int $passedDays);

    function print(Request $request, int $passedDays);

    function setRequestMethod(FarmRequestMethod $requestMethod);

    function getRequestMethod(Request $request,array $data): FarmRequestMethod;
}
