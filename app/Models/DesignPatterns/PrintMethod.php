<?php

namespace App\Models\DesignPatterns;

interface PrintMethod extends FarmRequestMethod
{
    function print($data);
}
