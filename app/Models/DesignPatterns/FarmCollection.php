<?php

namespace App\Models\DesignPatterns;

interface FarmCollection
{
    public static function fromCache();

    public static function fromXML();
}
