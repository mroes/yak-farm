<?php


namespace App\Models\DesignPatterns;

use App\Models\LabYak\LabYak;

interface LabYakState
{
    function milk(): ?float;

    function shave(): ?int;

    function daysUntilNextShave(): ?int;

    function canShaveLabYakByDay(int $passedDays): bool;

    function setContext(LabYak &$labYak);

    function getShavingDays(): array;

    function getNextShaveDay(): int;

    function growOld(): void;
}
