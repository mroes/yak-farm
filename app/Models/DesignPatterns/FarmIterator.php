<?php

namespace App\Models\DesignPatterns;

interface FarmIterator
{

    public function getNext();

    public function hasMore(): bool;
}
