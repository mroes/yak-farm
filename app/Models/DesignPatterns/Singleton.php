<?php

namespace App\Models\DesignPatterns;

trait Singleton
{

    protected static ?self $instance = null;

    abstract protected function __construct();

    abstract static function getInstance();
}
