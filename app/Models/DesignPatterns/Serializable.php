<?php

namespace App\Models\DesignPatterns;


interface Serializable
{
    public function serialize(): ?string;

    public function unserialize(string $serialized): void;
}
