<?php

namespace App\Models\DesignPatterns;

use Illuminate\Http\Request;

interface FarmRequestMethod
{
    function handle(Request $request);

     function respond(array $data);

    function getRequestPayloadData(): array;

    function setRequestPayloadData($data);
}
