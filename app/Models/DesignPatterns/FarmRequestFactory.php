<?php

namespace App\Models\DesignPatterns;


use App\Models\Farm\FarmBaseService;
use App\Models\LabYak\LabYakCollection;
use App\Models\Shop\GetHerdRequestMethod;
use App\Models\Shop\GetStockRequestMethod;
use App\Models\Shop\PrettyPrintFarmAssetsRequest;
use App\Models\Shop\ProcessMilkOrderRequest;
use App\Models\Shop\ProcessOrderRequest;
use App\Models\Shop\ProcessSkinOrderRequest;
use Illuminate\Http\Request;

class FarmRequestFactory
{

    use Singleton;

    protected function __construct(){}

    public static function getRequestProcessingMethod(Request $request, array $data): FarmRequestMethod
    {
        $canProcessFullOrder = false;
        $canProcessMilkOrder = false;
        $canProcessSkinsOrder = false;
        $canProcessOrder = false;
        if ($request->getMethod() == 'POST') {
            $ordered_data = $request->input('order');
            $canProcessMilkOrder = self::canProcessMilkOrder($ordered_data['milk'], $data['milk_liters_total']);
            $canProcessSkinsOrder = self::canProcessSkinsOrder($ordered_data['skins'], $data['skin_units_total']);
            $canProcessFullOrder = $canProcessMilkOrder && $canProcessSkinsOrder;
            $canProcessOrder = ($canProcessSkinsOrder || $canProcessMilkOrder);
        }
        if ($canProcessOrder) {
            if ($canProcessFullOrder) {
                $processingMethod = new ProcessOrderRequest();
            } else if ($canProcessMilkOrder && !$canProcessSkinsOrder) {
                $processingMethod = new ProcessMilkOrderRequest();
            } else if ($canProcessSkinsOrder && !$canProcessMilkOrder) {
                $processingMethod = new ProcessSkinOrderRequest();
            }
        } else if ($request->is('/yak-shop/shop/*')) {
            $processingMethod = new FaultyRequestMethod();
        } else if ($request->is('yak-shop/print/*') && isset($data['passedDays'])) {
            $processingMethod = new PrettyPrintFarmAssetsRequest();
            $processingMethod->setRequestPayloadData(FarmBaseService::getInstance()
                ->getPredictedStockDataByDayFromCollection(LabYakCollection::fromXML(),
                    $data['passedDays']));
        } else if ($request->is('yak-shop/stock/*') && isset($data['passedDays'])) {
            $processingMethod = new GetStockRequestMethod();
            $processingMethod->setRequestPayloadData($data);
        } else if ($request->is('yak-shop/herd/*') && isset($data['passedDays'])) {
            $processingMethod = new GetHerdRequestMethod();
            $data['collection'] = LabYakCollection::fromXML();
            $processingMethod->setRequestPayloadData($data);
        } else {
            /** @var @todo exception handling */
            $processingMethod = new FaultyRequestMethod();
        }
        return $processingMethod;
    }

    /**
     * @param $milk
     * @param $milk_liters_total
     * @return bool
     */
    public static function canProcessMilkOrder($milk, $milk_liters_total): bool
    {
        return ($milk == 0 || $milk <= $milk_liters_total);
    }

    /**
     * @param $skins
     * @param $skin_units_total
     * @return bool
     */
    public static function canProcessSkinsOrder($skins, $skin_units_total): bool
    {
        return ($skins == 0 || $skins <= $skin_units_total);
    }

    static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new self();
        }
        return static::$instance;
    }
}
