<?php

namespace App\Models\DesignPatterns;


use Illuminate\Http\Request;

class FaultyRequestMethod implements FarmRequestMethod
{
    function handle(Request $request)
    {
        return $this->respond([]);
    }

    function respond(array $data)
    {
        return response(__('Can not execute request'), 404);
    }

    function getRequestPayloadData(): array
    {
        // TODO: Implement getRequestPayloadData() method.
    }

    function setRequestPayloadData($data)
    {
        // TODO: Implement setRequestPayloadData() method.
    }
}
