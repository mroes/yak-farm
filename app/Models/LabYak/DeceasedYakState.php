<?php

namespace App\Models\LabYak;


class DeceasedYakState extends BaseLabYakState {

    public function milk(): ?float {
        return null;
    }

    public function shave(): ?int  {
        return null;
    }

    public function daysUntilNextShave(): ?int {
        return null;
    }

    public function canShaveLabYakByDay(int $passedDays): bool {
        return false;
    }

    public function setContext(LabYak &$labYak): DeceasedYakState {
        $this->context = $labYak;
        return $this;
    }

    public function getShavingDays(): array
    {
        return [];
    }

    public function getNextShaveDay(): int
    {
        return -1;
    }

    public function growOld(): void
    {
    }
}
