<?php

namespace App\Models\LabYak;


use App\Models\DesignPatterns\LabYakState;

class LabYak extends LabAnimal
{

    const PROP_NAME = 'name';
    const PROP_AGE = 'age';
    const PROP_SEX = 'sex';
    public static int $firstPossibleShavingDay = 100;
    private int $lastShavingDay;
    private LabYakState $state;
    public bool $hadFirstShaveJob = false;


    /**
     * array['fields']
     *          [fieldName]
     *              ['name']
     *              ['age']
     *              ['sex']
     * @param array $data (See above)
     *
     **/
    public function __construct(array $data)
    {
        [$name, $age, $sex] = array_values($data);
        $this->setName($name);
        $this->changeAge($age);

        $this->setSex($sex);
        if ($age >= static::$maxAgeInDays) {
            $this->changeState(new DeceasedYakState());
        } else {
            $this->changeState(new LivingYakState());
        }
    }

    /**
     * @return bool
     */
    public function eligibleForFirstShaveJob(): bool
    {
        return $this->getAgeInDays() >= static::$firstPossibleShavingDay && $this->hadFirstShaveJob == false;
    }
    /**
     * @param string $name
     * @return LabYak
     */
    public function setName(string $name): LabYak
    {
        $this->name = $name;
        return $this;

    }

    /**
     * @param $age
     */
    public function changeAge(float $age): void
    {
        if (is_float($age)) {
            $this->setAge($age);
        } else {
            $this->setAgeInDays($age);
        }
    }

    function setAge(float $age): void
    {
        $age = $age * 100;
        $this->age = $age > static::$maxAgeInDays ? static::$maxAgeInDays : $age;
        $this->drawLastsBreathOnMaxAgeReached();
    }

    function drawLastsBreathOnMaxAgeReached()
    {
        if (isset($this->state) && $this->maxAgeReached()) {
            $this->drawLastBreathAndDie();
        }
    }

    public function maxAgeReached(): bool
    {
        return $this->getAgeInDays() >= LabYak::$maxAgeInDays;
    }

    /**
     * @return int
     */
    public function getAgeInDays(): int
    {
        return $this->age;
    }

    public function drawLastBreathAndDie(): void
    {
        if ($this->isAlive()) {
            $this->changeState(new DeceasedYakState());
        }
    }

    public function isAlive(): bool
    {
        return (isset($this->state) && $this->state instanceof LivingYakState);
    }

    public function changeState(LabYakState $labYakState): void
    {
        $this->state = $labYakState;
        $this->syncStateContext();
    }

    public function syncStateContext()
    {
        if (isset($this->state)) {
            $this->state->setContext($this);
        }
    }

    /**
     * @param  $age
     * @return LabYak
     */
    public function setAgeInDays(int $age): void
    {
        $this->age = $age;
        $this->drawLastsBreathOnMaxAgeReached();
    }

    /**
     * @param string $sex
     * @return LabYak
     */
    public function setSex(string $sex): LabYak
    {
        $this->sex = $sex;
        return $this;
    }

    public function getShavingDays(): array
    {
        return $this->state->getShavingDays();
    }

    public function milk(): ?float
    {
        return $this->state->milk();
    }

    public function shave(): ?int
    {
        return $this->state->shave();
    }

    public function canShaveLabYakByDay(int $passedDays): bool
    {
        return $this->state->canShaveLabYakByDay($passedDays);
    }

    public function getNextShaveDay(): int
    {
        return $this->state->getNextShaveDay();
    }

    public function daysUntilNextShave(): ?int
    {
        return $this->state->daysUntilNextShave();
    }

    public function growOld(): void
    {
        if ($this->maxAgeReached()) {
            $this->drawLastBreathAndDie();
        } else {
            $this->age++;
        }
        $this->syncStateContext();
    }

    function getAge()
    {
        return $this->age / 100;
    }

    public function serialize()
    {
        $props = static::getAttributes();
        return serialize(array_map(fn($property_name) => $this->{$property_name}, $props));
    }

    public static function getAttributes()
    {
        return [static::PROP_NAME, static::PROP_AGE, static::PROP_SEX];
    }

    function toJson($options = 0)
    {
        return json_encode(['name' => $this->name, 'age' => $this->age / 100, 'sex' => $this->sex]);
    }



    public function unserialize($data)
    {
        list(
            $this->{static::PROP_NAME}, $this->{static::PROP_AGE}, $this->{static::PROP_SEX}
            ) = unserialize($data);
    }

    public function equals(self $other): bool
    {
        return (
            $this->getName() === $other->getName() &&
            $this->getAgeInDays() === $other->getAgeInDays() &&
            $this->getSex() === $other->getSex());
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSex(): string
    {
        return $this->sex;
    }

    public function getLastShavingDay(): int
    {
        return $this->lastShavingDay;
    }

    public function setLastShavingDay(int $day)
    {
        $this->lastShavingDay = $day;
        $this->syncStateContext();
    }

    public function setContext(LabYak &$labYak)
    {
    }
}
