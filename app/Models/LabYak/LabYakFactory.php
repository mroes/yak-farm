<?php

namespace App\Models\LabYak;


use App\Models\DesignPatterns\Singleton;

class LabYakFactory {


    use Singleton;

    protected ?LabYak $model = null;

    protected function __construct() {}

    public static function getInstance(): self {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @return string
     * @throws \Exception
     */
    function simpleGenericName():string {
       return 'Betty-' . random_int(7, 999999);
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array {
        return [
            'name' => $this->simpleGenericName(),
            'age' => 0,
            'sex' => 'm'
        ];
    }

    public function default(): LabYak {
        return new LabYak($this->definition());
    }

    public function create(): LabYak{
        if(is_null($this->model)){
            $this->model = $this->default();
        }
        return clone $this->model;
    }

    public function canCreate($data): bool {
        return LabYak::getAttributes() == array_keys($data);
    }

    public function createFrom(array $data): ?LabYak {
        if($this->canCreate($data)){
            $this->model = new LabYak($data);
        }else{
            $this->create();
        }
        return clone $this->model;
    }
}
