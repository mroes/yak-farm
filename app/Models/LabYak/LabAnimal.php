<?php

namespace App\Models\LabYak;


use App\Models\DesignPatterns\LabYakState;
use Illuminate\Database\Eloquent\Model;

abstract class LabAnimal implements LabYakState {

    public static int $maxAgeInDays = 1000;
    protected string $name;
    protected int $age;
    protected string $sex;
}
