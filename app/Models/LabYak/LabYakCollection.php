<?php

namespace App\Models\LabYak;

use App\Models\DesignPatterns\FarmCollection;
use Illuminate\Support\Collection;

class LabYakCollection extends Collection implements FarmCollection
{

    /**
     * @return Collection
     */
    public static function fromXML(): Collection
    {
        $xmlString = file_get_contents(public_path('yak.xml'));
        $herd = simplexml_load_string($xmlString);
        $ar = [];
        foreach ($herd->labyak as $labyak) {
            $ar[] = LabYakFactory::getInstance()->createFrom([
                    'name' => (string)$labyak['name'],
                    'age' => (float)$labyak['age'],
                    'sex' => (string)$labyak['sex']
                ]
            );
        }

        return collect($ar);
    }

    public static function fromCache()
    {
        // TODO: Implement fromCache() method.
    }
}
