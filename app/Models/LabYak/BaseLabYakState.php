<?php


namespace App\Models\LabYak;


use App\Models\DesignPatterns\LabYakState;

abstract class BaseLabYakState implements LabYakState
{
    protected LabYak $context;
}
