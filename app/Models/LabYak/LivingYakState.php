<?php

namespace App\Models\LabYak;


class LivingYakState extends BaseLabYakState
{

    public function milk(): float
    {
        return 50 - $this->context->getAgeInDays() * 0.03;
    }

    public function shave(): int
    {
        $this->context->setLastShavingDay($this->context->getAgeInDays());
        return 1;
    }

    public function canShaveLabYakByDay(int $passedDays): bool
    {
        return ($passedDays === ($this->daysUntilNextShave()));
    }

    public function daysUntilNextShave(): ?int
    {
        if ($this->context->getAgeInDays() >= LabYak::$firstPossibleShavingDay) {
            $nextShavingDay = ceil(8 + $this->context->getAgeInDays() * 0.01);
        } else {
            $nextShavingDay = LabYak::$firstPossibleShavingDay;
        }
        return $nextShavingDay;
    }

    /**
     * @param LabYak $labYak
     * @return LivingYakState
     */
    public function setContext(LabYak &$labYak): LivingYakState
    {
        $this->context = $labYak;
        return $this;
    }

    public function getShavingDays(): array
    {
        $shavingDays = [];
        $shaveYakOnDay = $this->getNextShaveDay();
        for ($i = $this->context->getAgeInDays(); $i < LabYak::$maxAgeInDays; $i++) {
            if ($shaveYakOnDay === $i) {
                $shavingDays[] = $this->context->getAgeInDays() + 1;
                $shaveYakOnDay = $this->getNextShaveDay();
            }
            $this->growOld();
        }
        return $shavingDays;
    }

    public function getNextShaveDay(): int
    {
        $nextShavingDay = $this->daysUntilNextShave();
        return $nextShavingDay + $this->context->getAgeInDays();
    }

    public function growOld(): void
    {
        $this->context->setAgeInDays(($this->context->getAgeInDays() + 1));
        if ($this->context->maxAgeReached() && $this->context->isAlive()) {
            $this->context->changeState(new DeceasedYakState());
        }
    }
}
