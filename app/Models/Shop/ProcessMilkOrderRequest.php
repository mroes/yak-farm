<?php

namespace App\Models\Shop;


use App\Models\DesignPatterns\FarmRequestMethod;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProcessMilkOrderRequest extends BaseFarmRequestBehaviour implements FarmRequestMethod
{

    function handle(Request $request): JsonResponse
    {
        return $this->respond(['milk' => $request->input('order.milk')]);
    }

    function respond(array $data): JsonResponse
    {
        return response()->json($data, 206);
    }
}
