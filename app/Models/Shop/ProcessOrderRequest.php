<?php

namespace App\Models\Shop;


use App\Models\DesignPatterns\FarmRequestMethod;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProcessOrderRequest extends BaseFarmRequestBehaviour implements FarmRequestMethod
{

    function handle(Request $request): JsonResponse
    {
        return $this->respond($request->input('order'));
    }

    function respond(array $data): JsonResponse
    {
        return response()->json($data, 201);
    }
}
