<?php

namespace App\Models\Shop;


use App\Models\DesignPatterns\FarmRequestMethod;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class GetStockRequestMethod extends BaseFarmRequestBehaviour implements FarmRequestMethod
{

    function handle(Request $request): JsonResponse
    {
        return $this->respond(['milk' => number_format($this->getRequestPayloadData()['milk_liters_total'], 2, ".", ''),
            'skins' => $this->getRequestPayloadData()['skin_units_total']]);
    }

    function respond(array $data): JsonResponse
    {
        return response()->json($data);
    }
}


