<?php

namespace App\Models\Shop;


use App\Models\DesignPatterns\FarmRequestMethod;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class GetHerdRequestMethod extends BaseFarmRequestBehaviour implements FarmRequestMethod
{

    function handle(Request $request): JsonResponse
    {
        $payload = $this->getRequestPayloadData();
        return $this->respond(['herd' => $payload['herd']]);
    }

    function respond(array $data)
    {
        return response()->json($data);
    }
}


