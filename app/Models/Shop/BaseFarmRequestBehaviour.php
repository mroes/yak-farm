<?php

namespace App\Models\Shop;


use App\Models\DesignPatterns\FarmRequestMethod;

abstract class BaseFarmRequestBehaviour implements FarmRequestMethod
{
    protected array $requestPayloadData = [];

    public function getRequestPayloadData(): array
    {
        return $this->requestPayloadData;
    }

    public function setRequestPayloadData($data)
    {
        $this->requestPayloadData = $data;
    }

}
