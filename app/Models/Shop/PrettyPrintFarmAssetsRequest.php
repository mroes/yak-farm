<?php

namespace App\Models\Shop;

use App\Models\DesignPatterns\FarmRequestMethod;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PrettyPrintFarmAssetsRequest extends PrettyPrintRequestBehaviour implements FarmRequestMethod
{

    /**
     * @param Request $request
     * @return Application|ResponseFactory|Response
     */
    function handle(Request $request)
    {

        $data = $this->getRequestPayloadData();
        $milk_amount = $data['milk_liters_total'] ?? 0.000;
        $skin_units = $data['skin_units_total'] ?? 1;
        $response = __('In Stock') . ':';
        $response .= "\n";
        $response .= "<br/>";
        $response .= str_repeat('&nbsp;', 5) . number_format($milk_amount, 3) . ' ' . __("liters of milk");
        $response .= "\n";
        $response .= str_repeat('&nbsp;', 5) . $skin_units . ' ' . __("liters of milk");
        $response .= str_repeat("\n", 2);
        $response .= __('Herd') . ':';
        $response .= "\n";
        $response .= "<br/>";
        foreach ($data['herd'] as $labYakRow) {
            $response .= str_repeat('&nbsp;', 5) . $labYakRow['name'] . ' ' . $labYakRow['age'] . __("years old");
            $response .= "\n";
        }
        $this->print($response);
        return $this->respond([$response]);
    }


    function respond(array $data)
    {
        return response($data[0]);

    }
}
