<?php

namespace App\Models\Shop;

use App\Models\DesignPatterns\FarmRequestMethod;

abstract class PrettyPrintRequestBehaviour extends BaseFarmRequestBehaviour implements FarmRequestMethod
{
    function print($data){echo $data;}
}
