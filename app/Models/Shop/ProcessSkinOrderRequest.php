<?php

namespace App\Models\Shop;


use App\Models\DesignPatterns\FarmRequestMethod;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProcessSkinOrderRequest extends BaseFarmRequestBehaviour implements FarmRequestMethod
{

    function handle(Request $request): JsonResponse
    {
        $milk_ordered = $request->input('order.skins');
        return $this->respond(['skins' => $milk_ordered]);
    }

    function respond(array $data): JsonResponse
    {
        return response()->json($data, 206);
    }
}
