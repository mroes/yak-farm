<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>MFM Store</title>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
<nav class="teal white-text" role="navigation">
    <div class="nav-wrapper container">
        <a id="logo-container" href="#" class="white-text brand-logo">MFM</a>
        <ul class="right hide-on-med-and-down">
            <li><a class="white-text" href="#">Home</a></li>
        </ul>

        <ul id="nav-mobile" class="sidenav">
            <li><a href="#">Home</a></li>
        </ul>
        <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
</nav>

<div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
        <div class="container">
            <br><br>
            <h1 class="header center teal-text text-lighten-1">MFM Store</h1>
            <div class="row center">
                <h5 class="header col s12 light">World's most magnificent fur & milk store</h5>
            </div>

            <br><br>

        </div>
    </div>
    <div class="parallax"><img src="img/closeup-shot-head-shaggy-yak.jpg" alt="Unsplashed background img 1"></div>
</div>


<div class="container">
    <div class="section">

        <div class="row">
            <div class="col s12 center">
                <h3><i class="mdi-content-send brown-text"></i></h3>
                <h4>Contact Us</h4>
                <p class="left-align light">
                    Lab Yaks were domesticated in the Qinghai-Tibetan Underground Lab about 4,500 years ago. Just as the buffalo sustained Native Americans, the yak provided Tibetan tribes with meat, milk, butter, cheese, wool, leather and fuel.
                    These beautiful animals have a reputation of being calmer than bison but smarter than cattle. An interesting discovery was that yak are more closely related to the American bison than the European bison.
                    Yak evolved in the harshest of environments, the Himalayan labs. The deepest point on Earth, Cave Rupture Farm, is located there at 29,029 feet under sea level!</p>
            </div>
        </div>

    </div>

</div>


<div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row center">
                <h5 class="header col s12 light"> World's Magnificent Fur & Milk</h5>
            </div>
        </div>
    </div>
    <div class="parallax"><img src="img/fur.webp" alt="Unsplashed background img 2"></div>
</div>

<div class="container">
    <div class="section">

        <!--   Icon Section   -->
        <div class="row">

            @foreach ($herd as $labyak)
                <div class="col s4 m4">
                    <div class="card">
                        <div class="card-image">
                            <img src="{{$loop->even ? 'img/closeup-shot-head-shaggy-yak-2.jpg' : 'img/betty-2.jpg'}}">
                            <span class="card-title"><?= $labyak->getName()?></span>
                        </div>
                        <div class="card-content">
                            <p>age: <?=$labyak->getAge()?></p>
                            <p>sex: <?= strtolower($labyak->getSex()) === 'm' ? 'Male' : 'Female' ?></p>
                        </div>
                        <div class="card-action">
                            <button data-target="modal1" class="btn modal-trigger">Buy</button>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>

    </div>
</div>


<div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row center">
                <h5 class="header col s12 light">A modern responsive front-end framework based on Material Design</h5>
            </div>
        </div>
    </div>
    <div class="parallax"><img src="img/grass-field.jpg" alt="grass-field img 3"></div>
</div>
<footer class="page-footer teal">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">Company Bio</h5>
                <p class="grey-text text-lighten-4">Rupture Farm invites you to reconnect to the unmanipulated genetics of this ancient breed that nourished humanity for millennia – and enjoy superb flavor, as well.
                    Yak milk can easily catch fire on the grill due to all the healthy omega-3 oils, its fur will keep you warm all through the year.</p>


            </div>
            <div class="col l3 s12">
                <h5 class="white-text">Settings</h5>
                <ul>
                    <li><a class="white-text" href="#!">Link 1</a></li>
                    <li><a class="white-text" href="#!">Link 2</a></li>
                    <li><a class="white-text" href="#!">Link 3</a></li>
                    <li><a class="white-text" href="#!">Link 4</a></li>
                </ul>
            </div>
            <div class="col l3 s12">
                <h5 class="white-text">Connect</h5>
                <ul>
                    <li><a class="white-text" href="#!">Link 1</a></li>
                    <li><a class="white-text" href="#!">Link 2</a></li>
                    <li><a class="white-text" href="#!">Link 3</a></li>
                    <li><a class="white-text" href="#!">Link 4</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="footer-copyright">
        <div class="container">
            Made by <span class="brown-text text-lighten-3" >Michael Roes</span>
        </div>
    </div>
</footer>
<div id="modal1" class="modal">
    <div class="modal-content">
        <h4>Check out</h4>
        <form method="POST" id="check-out-form" action="/yak-shop/order">
            @csrf
            <div class="row">
                <div class="input-field col s7">
                    <input required placeholder="Fill in your name" id="inpCustomer" type="text" class="validate">
                    <label for="first_name">Name</label>
                </div>
                <div class="input-field col s6">
                    <input required pattern="[0-9]+" type="number" id="inpDay" name="input_milk" step="1" min="0" max="9000" />
                    <label for="inpDay">Day</label>
                </div>
                <div class="input-field col s6">
                        <input pattern="[0-9]+" type="number" id="inpMilk" name="input_milk" step=".1" min="0" max="9000" />
                        <label for="inpMilk">Milk in liters</label>
                </div>
                <div class="input-field col s6">
                    <input pattern="[0-9]+" type="number" id="inpSkins" name="input_skins" step="1" min="0" max="9000" />
                    <label for="inpSkins">Skins in units</label>
                </div>
                <div class="input-field col s12">
                    <p class="notification-field">

                    </p>
                </div>

            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn waves-effect waves-light" id="inpSubmit" type="submit" name="action">Buy
            <i class="material-icons right">add_shopping_cart</i>
        </button>
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancel</a>
    </div>
</div>

<!--  Scripts-->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="js/materialize.js"></script>
<script src="js/init.js"></script>
<script>
    function isNumberKey(evt)
    {
        const charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
    $(document).ready(function(){
        $('.modal').modal();

        $('#inpMilk').on('keypress', isNumberKey);
        $('#inpSkins').on('keypress', isNumberKey);

        function btnInputSubmitClickHandler(){
            $('#inpSubmit').on('click', function(e){
                const that = this;
                e.preventDefault();
                const days = $('#inpDay').val();
                const data = {
                    _token: $('input[name="_token"]').val(),
                    order:{
                        milk : $('#inpMilk').val(),
                        skins : $("#inpSkins").val(),
                    },
                    customer : $("#inpCustomer").val()
                }
                if(data.order.milk === "" && data.order.skins === ""){
                    $(".notification-field").html(`No products selected`)
                    return false;
                }
                if( days === "" || days === 'undefined'){
                    $(".notification-field").html(`No day selected`);

                    return false;
                }
                $(this).unbind('click');
                $.ajax({
                    type: 'POST',
                    url: "yak-shop/order/" + days,
                    data: data,
                    success: function(response){
                        console.log(response)
                        $(".notification-field").html(`<code>${JSON.stringify(response)}</code>`)
                    },complete: function(response){
                        btnInputSubmitClickHandler();

                    },error: function(error){
                        console.dir(error);
                        $(that).unbind('click');
                        btnInputSubmitClickHandler();
                        $(".notification-field").html(`<code>${JSON.stringify(error)}</code>`)
                    }
                })
            })

        }
        btnInputSubmitClickHandler();
    });
</script>
</body>
</html>
