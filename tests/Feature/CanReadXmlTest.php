<?php

namespace Tests\Feature;


use App\Models\LabYak\LabYakCollection;
use Tests\TestCase;

class CanReadXmlTest extends TestCase
{

    /** @test */
    public function canReadFromXML()
    {
        $labCollection = LabYakCollection::fromXML();
        $firstLabYakName = $labCollection->first();
        $this->assertEquals('Betty-1', $firstLabYakName->getName());
    }

    /** @test */
    public function canCreateLabYakCollectionFromXML()
    {
        $labCollection = LabYakCollection::fromXML();
        $this->assertNotFalse($labCollection);
    }
}
