<?php

namespace Tests\Feature;

use App\Models\Farm\FarmBaseService;
use App\Models\LabYak\LabYakCollection;
use Tests\TestCase;

class OutputFarmAssetsTest extends TestCase
{


    /** @test */
    function canPrintStockByDay()
    {
        $herdCollection = LabYakCollection::fromXML();
        $stockData = FarmBaseService::getInstance()->getPredictedStockDataByDayFromCollection($herdCollection, 13);
        $this->assertNotEmpty($stockData);
    }


    /** @test */
    function canPrettyPrintFarmAssetsByDay()
    {
        $response = $this->get('/yak-shop/print/23');
        $response->assertStatus(200)
            ->assertSeeText('Herd');
    }
}
