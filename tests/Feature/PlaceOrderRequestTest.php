<?php

namespace Tests\Feature;

use Tests\TestCase;

class PlaceOrderRequestTest extends TestCase
{

    /** @test */
    public function canOrderItemsInStockReturnsJsonResponse()
    {
        $response = $this->postJson('/yak-shop/shop/14',
            [
                'customer' => 'Medvedev',
                'order' => ['milk' => '1100', 'skins' => 3]

            ]);

        $response
            ->assertStatus(201)
            ->assertJson(['milk' => '1100', 'skins' => 3]);
    }

    /** @test */
    public function canOrderSomeItemsInStockReturnsJsonResponse()
    {
        $response = $this->postJson('/yak-shop/shop/14',
            [
                'customer' => 'Medvedev',
                'order' => ['milk' => '1200', 'skins' => 3]

            ]);

        $response
            ->assertStatus(206)
            ->assertJson(['skins' => 3]);
    }


    /** @test */
    public function canNotOrderItemsOutOfStockReturnsJsonResponse()
    {
        $this->withExceptionHandling();
        $response = $this->postJson('/yak-shop/shop/14',
            [
                'customer' => 'Medvedev',
                'order' => ['milk' => '11000', 'skins' => 3444]
            ]);
        $response
            ->assertStatus(404);
    }

}
