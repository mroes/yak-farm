<?php

namespace Tests\Feature;

use Tests\TestCase;

class GetHerdRequestTest extends TestCase
{

    /** @test */
    function canProcessGetHerdRequest()
    {
        $response = $this->get('/yak-shop/herd/13');
        $response->assertStatus(200)
            ->assertJsonPath('herd.0.name', "Betty-1")
            ->assertJsonPath('herd.0.age', 4.13)
            ->assertJsonPath('herd.0.age-last-shaved', '4.0');
    }

}
