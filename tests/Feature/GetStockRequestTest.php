<?php

namespace Tests\Feature;

use Tests\TestCase;

class GetStockRequestTest extends TestCase
{

    /** @test */
    function canProcessGetStockRequest()
    {
        $response = $this->get('/yak-shop/stock/13');
        $response->assertStatus(200)
            ->assertJson(['milk' => '1104.48', 'skins' => 3]);
    }

}
