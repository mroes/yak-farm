<?php

namespace Tests\Unit;

use App\Models\LabYak\LabYak;
use App\Models\LabYak\LabYakCollection;
use App\Models\LabYak\LabYakFactory;
use Tests\TestCase;

class LabYakTest extends TestCase
{

    /**
     * @test
     */
    public function canInitiateLabYak()
    {
        $this->assertInstanceOf(
            LabYak::class,
            LabYakFactory::getInstance()->createFrom(['name' => "Betty-5", 'age' => 4.0, 'sex' => "m"])
        );
    }


    /** @test */
    public function canSerializeYak()
    {
        $labyak = LabYakCollection::fromXML()->first();
        $serialized = $labyak->serialize();
        $this->assertNotEmpty($serialized);
    }

    /** @test */
    public function canUnserialize()
    {
        $data = serialize(["Betty-5", 400, "m"]);
        $labyakProto = LabYakFactory::getInstance()->createFrom(['name' => "Betty-5", 'age' => 4.0, 'sex' => "m"]);
        $labyak = LabYakCollection::fromXML()->first();
        $labyak->unSerialize($data);
        $this->assertObjectEquals($labyak, $labyakProto);
    }

    /** @test */
    public function canConvertToJson()
    {
        $labyak = LabYakCollection::fromXML()->first();
        $labyakJson = $labyak->toJson();
        $this->assertJson($labyakJson);
    }

    /** @test */
    public function convertedJsonDataContainsExpectedProperties()
    {
        $labyak = LabYakCollection::fromXML()->first();
        $data = json_decode($labyak->toJson(), true);
        $this->assertArrayHasKey('name', $data);
        $this->assertArrayHasKey('age', $data);
        $this->assertArrayHasKey('sex', $data);
    }

    /** @test */
    public function labYakIsAliveWhenSpawned()
    {
        $labYak = LabYakFactory::getInstance()->createFrom(['name' => "Betty-6", 'age' => "0", 'sex' => "m"]);
        $this->assertTrue($labYak->isAlive());
    }

    /** @test */
    public function labYakDiesOnAgeOfTen()
    {
        $labYak = LabYakFactory::getInstance()->createFrom(['name' => "Betty-6", 'age' => "0", 'sex' => "m"]);
        $labYak->changeAge(11.0);
        $this->assertEquals(LabYak::$maxAgeInDays, $labYak->getAgeInDays());
        $this->assertFalse($labYak->isAlive());
    }

    /** @test */
    public function canMilkFromLivingLabYak()
    {
        $labyak = LabYakCollection::fromXML()->first();
        $milk = $labyak->milk();
        $this->assertIsFloat($milk);
    }

    /** @test */
    public function canCalculateExtractedMilkFromLivingLabYak()
    {
        $labyak = LabYakCollection::fromXML()->first();
        $milk = $labyak->milk();
        $this->assertEquals(38.0, $milk);
    }

    /** @test */
    public function oneYearOldLabYakProducesPredictedAmountOfMilkByDaysPeriod()
    {
        $labYak = LabYakFactory::getInstance()->createFrom(["name" => "Betty-007", "age" => "1.0", "sex" => "f"]);
        $this->assertEquals(100, $labYak->getAgeInDays());
        $this->assertEquals(47, $labYak->milk());
        $milk[] = 0;
        for ($i = 0; $i < 2; $i++) {
            $milk[] = $labYak->milk();
            $labYak->growOld();
        }
        $this->assertEquals(93.97, array_sum($milk));
    }

    /** @test */
    public function canShaveLivingLabYakOnFirstDay()
    {
        $labyak = LabYakCollection::fromXML()->first();
        $canShave = $labyak->canShaveLabYakByDay(12);
        $this->assertTrue($canShave);
    }

    /** @test */
    public function canPredictLabYakShavingDaysUntillDeath()
    {
        $labYak = LabYakFactory::getInstance()->createFrom(["name" => "Betty-007", "age" => "9.0", "sex" => "f"]);
        $shavingDays = $labYak->getShavingDays();
        $expectedDays = [918, 936, 954, 972, 990];
        $this->assertTrue($expectedDays == $shavingDays);
    }

    /** @test */
    public function canPredictLabYakShaveDayByAge()
    {
        $labYak = LabYakCollection::fromXML()->first();
        $nextShavingDay = $labYak->getNextShaveDay();
        $this->assertEquals(412, $nextShavingDay);
    }

    /** @test */
    public function deceasedLabYakShouldNotProduceMilk()
    {
        $labYak = LabYakFactory::getInstance()->create();
        $labYak->drawLastBreathAndDie();
        $this->assertNull($labYak->milk());
    }

    /** @test */
    public function deceasedLabYakShouldNotProduceSkinUnits()
    {
        $labYak = LabYakFactory::getInstance()->create();
        $labYak->drawLastBreathAndDie();
        $this->assertNull($labYak->shave());
        $this->assertFalse($labYak->isAlive());
    }

}
