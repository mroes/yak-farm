<?php

use App\Models\Farm\FarmBaseService;
use App\Models\LabYak\LabYakCollection;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/shop', function () {
    return view('shop',[
        'herd' => LabYakCollection::fromXML()
    ]);
});



Route::post('/yak-shop/order/{passedDays}', [FarmBaseService::class, 'processOrder' ]);

Route::post('/yak-shop/shop/{passedDays}', [FarmBaseService::class, 'processOrder' ]);

Route::get('/yak-shop/print/{passedDays}', [FarmBaseService::class, 'print' ]);

Route::get('/yak-shop/herd/{passedDays}', [FarmBaseService::class, 'processOrder' ]);

Route::get('/yak-shop/stock/{passedDays}', [FarmBaseService::class, 'processOrder' ]);

Route::get('/diagram', function () {
    return view('diagram');
});

